import React from "react";
import { createRoot } from "react-dom/client";
import { useEffect } from "react";
import App from './src/App';

const container = document.getElementById("root")
const root = createRoot(container);
function AppWithCallbackAfterRender() {
    useEffect(() => {
      //console.log('rendered');
    });
  
    return <App />
  }

root.render(<AppWithCallbackAfterRender />);
