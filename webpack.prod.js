const webpack = require('webpack');
const { merge } = require('webpack-merge')
const common = require('./webpack.common.js');
const path = require('path');

const {
    CleanWebpackPlugin
} = require('clean-webpack-plugin');

const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');

const MODE = 'production';

module.exports = merge(common, {
    mode: MODE,
    devtool: 'source-map',
    module: {
        rules: [{
            test: /\.(scss|sass)$/,
            use: [
                MiniCssExtractPlugin.loader,
                "css-loader",
                "sass-loader"
            ]
        }]
    },
    plugins: [
        // nó sẽ clean thư mục`dist/js & dist/css` trước khi build 
        new CleanWebpackPlugin({
            cleanOnceBeforeBuildPatterns: ['./dist/js', './dist/css'],
        }),
        // nó sẽ minify file css
        new MiniCssExtractPlugin({
            filename: "css/index.css"
        }),
        new HtmlWebpackPlugin({
            template: path.join(__dirname, "public", "index.html"),
        }),
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery"
        })
    ]
});