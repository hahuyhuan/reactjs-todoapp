import { actions } from '../constants/TodoConstants';
import { TodoInit } from '../constants/TodoConstants';

const TodoReducer = (state = TodoInit, action) => {
    var payload = action.payload;
    switch (action.type) {
        case actions.ADD_TODO:
            console.log(actions.ADD_TODO);
            return [
                ...state,
                {
                    id: payload.id,
                    title: payload.title,
                    priority: payload.priority,
                    completed: payload.completed
                }
            ];

        case actions.DELETE_TODO:
            console.log(actions.DELETE_TODO);
            return state.filter((todo) => todo.id !== payload);

        case actions.GET_TODO:
            console.log(actions.GET_TODO);
            return state.map((todo) => todo.id === payload.id ? { ...state, completed: !todo.completed }
                : todo);

        case actions.UPDATE_TODO:
            console.log(actions.UPDATE_TODO);
            return state.map((todo) => {
                if (todo.id === payload.id) {
                    return {
                        ...todo,
                        id: payload.id,
                        title: payload.title,
                        priority: payload.priority,
                        completed: payload.completed
                    }
                } else {
                    return todo;
                }
            });

        case actions.TOGGLE_TODO_PRIORITY:
            console.log(actions.TOGGLE_TODO_PRIORITY);
            return state.map((todo) => {
                if (todo.id === payload) {
                    return {
                        ...todo,
                        completed: !todo.completed
                    }
                } else {
                    return todo;
                }
            });

        default:
            return state;
    }
}

export { TodoReducer };