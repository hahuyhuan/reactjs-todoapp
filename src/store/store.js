import { createStore } from 'redux';
import { TodoInit } from '../constants/TodoConstants';
import { TodoReducer } from '../reducers/TodoReducer';

let store = createStore(TodoReducer);

export default store;