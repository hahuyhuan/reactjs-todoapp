import { actions } from '../constants/TodoConstants';

export const addTodo = (payload) => ({
    type: actions.ADD_TODO,
    payload,
});

export const deleteTodo = (payload) => ({
    type: actions.DELETE_TODO,
    payload,
});

export const getTodo = (payload) => ({
    type: actions.GET_TODO,
    payload,
});

export const updateTodo = (payload) => ({
    type: actions.UPDATE_TODO,
    payload,
});

export const toggleTodoPriority = (payload) => ({
    type: actions.TOGGLE_TODO_PRIORITY,
    payload,
});
