// import logo from './logo.svg';
import { Component, useState } from "react";
import TodoContainer from "./containers/Todo/TodoContainer";
import { Provider } from "react-redux";
import store from "./store/store";

function App() {
    return (
        <>
            <Provider store={store}>
                <TodoContainer></TodoContainer>
            </Provider>
            {/* <TodoProvider todoInit={TodoInit2}>
                <TodoContainer></TodoContainer>
            </TodoProvider> */}
        </>
    );
}

export default App;
