const priorityLevel = [
  {
      label: 'None',
      decoration: '<span class="priority-decoration">None</span>'
  },
  {
      label: 'Low',
      decoration: '<span class="priority-decoration -low">Low</span>'
  },
  {
      label: 'Medium',
      decoration: '<span class="priority-decoration -medium">Medium</span>'
  },
  {
      label: 'High',
      decoration: '<span class="priority-decoration -high">High</span>'
  }
];

export { priorityLevel };
