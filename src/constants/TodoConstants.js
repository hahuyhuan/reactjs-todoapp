export const actions = {
    ADD_TODO: "ADD_TODO",
    DELETE_TODO: "DELETE_TODO",
    GET_TODO: "GET_TODO",
    UPDATE_TODO: "UPDATE_TODO",
    TOGGLE_TODO_PRIORITY: "TOGGLE_TODO_PRIORITY",
};

export const TodoInit = [
    {
        id: 0,
        title: "Learn ReactJs",
        priority: 1,
        completed: true
    },
    {
        id: 1,
        title: "Learn AngularJS",
        priority: 2,
        completed: true
    },
    {
        id: 2,
        title: "Learn VueJS",
        priority: 3,
        completed: false
    }
]


export const TodoInit2 = [
    {
        id: 0,
        title: "Learn ReactJszzz",
        priority: 2,
        completed: true
    },
    {
        id: 1,
        title: "Learn AngularJSzzz",
        priority: 3,
        completed: true
    },
    {
        id: 2,
        title: "Learn VueJSzz",
        priority: 3,
        completed: false
    }
]