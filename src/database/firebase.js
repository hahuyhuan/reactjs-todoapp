// Import the functions you need from the SDKs you need
import { initializeApp } from 'firebase/app';
import { getFirestore, collection, getDocs, setDoc, doc, updateDoc, deleteDoc } from 'firebase/firestore/lite';
import { v4 as uuidv4 } from "uuid";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDkN0LX4cMt0WDKUHt6TcY9fU3zE4rrqGs",
  authDomain: "webapptesting-68ce8.firebaseapp.com",
  projectId: "webapptesting-68ce8",
  storageBucket: "webapptesting-68ce8.appspot.com",
  messagingSenderId: "559615010694",
  appId: "1:559615010694:web:12c8e9662baea6e97bae07",
  measurementId: "G-FP56QDXX99"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

// Get a list of cities from your database
async function getTasksFromData() {
  const TasksCol = collection(db, 'tasks');
  const taskSnapshot = await getDocs(TasksCol);
  console.log(taskSnapshot.docs);
  const taskList = taskSnapshot.docs.map( (task) => {
    return {...task.data(), id: task.id}}
  );
  
  return taskList;
}

async function createTaskToData(task) {
  await setDoc(doc(db, "tasks", uuidv4()), {
    title: task.title,
    priority: task.priority,
    completed: task.completed
  });
}

async function updateTaskToData(task) {
  await updateDoc(doc(db, "tasks", task.id), {
    title: task.title,
    priority: task.priority,
    completed: task.completed
  });
}

async function deleteTaskFromData(task) {
  await deleteDoc(doc(db, "tasks", task.id));
}


export { getTasksFromData, createTaskToData, updateTaskToData, deleteTaskFromData };