function DropdownItem(props) {
  // console.log(props);
  const attr = {
    "value": props.value,
    "is-selected" : props.selected,
    "onClick" : props.onClick
  }
  return (
    <li className="dropdown__item" {...attr}>{props.children}</li>
  )
}

export default DropdownItem;