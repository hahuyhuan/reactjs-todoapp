import InputText
 from "../InputText";
function FilterItemText({ label = "Filter by", value, onChangeValue, placeholder="Nhập tên task", className="add-task-input"}) {
    return (
        <div className="filter-item -full-width">
            <label>{label}</label>
            <InputText
                className={className}
                placeholder={placeholder}
                value={value}
                onChange={onChangeValue}
            />
        </div>
    );
}

export default FilterItemText;