import { priorityLevel } from "../../constants/priority";

function Item({
    todo,
    handleOnChangeCompleted,
    handleClickDelete,
    handleClickEdit,
}) {
    return (
        <tr>
            <td>
                {todo.completed}
                {todo.completed ? <del>{todo.title}</del> : todo.title}
            </td>
            <td
                dangerouslySetInnerHTML={{
                    __html: priorityLevel[todo.priority].decoration,
                }}
            ></td>
            <td>
                <input
                    type="checkbox"
                    onChange={handleOnChangeCompleted}
                    checked={todo.completed}
                />
            </td>
            <td>
                <button onClick={handleClickDelete}>Delete</button>
                <button onClick={handleClickEdit}>Edit</button>
            </td>
        </tr>
    );
}

export default Item;
