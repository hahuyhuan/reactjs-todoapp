import InputText from "../InputText";
import Dropdown from "../Dropdown";
import DropdownItem from "../DropdownItem";

function FormTask({
    inputTitleValue,
    inputTitleOnChange,
    inputPriorityValue,
    inputPriorityOnchange,
    onClickAdd,
    onClickDone,
    onClickCancel,
    isEditing,
    errorContent,
    labelDoneBtn = "Done",
    labelCancelBtn = "Cancel",
    labelAddBtn = "Add Task",
    
}) {
    const Actions = (e) => {
        if (isEditing === true) {
            return (
                <>
                    <button onClick={onClickDone}>{labelDoneBtn}</button>
                    <button onClick={onClickCancel}>{labelCancelBtn}</button>
                </>
            );
        } else {
            return <button onClick={onClickAdd}>{labelAddBtn}</button>;
        }
    };

    return (
        <>
            <section className="add-task">
                <InputText
                    className="add-task-input"
                    placeholder="Nhập tên task"
                    value={inputTitleValue}
                    onChange={inputTitleOnChange}
                />
                <Dropdown
                    value={inputPriorityValue}
                    onChange={inputPriorityOnchange}
                >
                    <DropdownItem value="0">Priority</DropdownItem>
                    <DropdownItem value="1">Low</DropdownItem>
                    <DropdownItem value="2">Medium</DropdownItem>
                    <DropdownItem value="3">High</DropdownItem>
                </Dropdown>
                <Actions></Actions>
                {errorContent && (
                    <div
                        className="errors"
                        dangerouslySetInnerHTML={{ __html: errorContent }}
                    ></div>
                )}
            </section>
        </>
    );
}

export default FormTask;
