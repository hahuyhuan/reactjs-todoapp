import { priorityLevel } from "../../constants/priority";
import InputText from "../InputText";
import DropdownContainer from "../../containers/DropdownContainer";
import DropdownItem from "../DropdownItem";

function Filter2({
    filter,
    handleOnChangeSearchText,
    handleOnChangeSort,
    handleOnChangePriority,
    handleOnChangeStatus,
}) {
    return (
        <>
            <section className="filter" style={{background:"#ccc"}}>
                <div className="filter-item -full-width">
                    <label>Tìm kiếm</label>
                    <input type="text"  placeholder="Nhập tên task" value={filter.searchText} onChange={handleOnChangeSearchText} />
                </div>
                <div className="filter-item">
                    <label>Sort</label>
                    <select value={filter.sort} onChange={handleOnChangeSort}>
                        <option value="latest">Latest</option>
                        <option value="alphabet">alphabet</option>
                    </select>
                </div>
                <div className="filter-item">
                    <label>Priority</label>
                    <select value={filter.priority} onChange={handleOnChangePriority}>
                        {priorityLevel.map((item, index) => {
                            return (
                                <option key={index} value={index}>
                                    {item.label}
                                </option>
                            );
                        })}
                    </select>
                </div>
                <div className="filter-item">
                    <label>Status</label>
                    <select value={filter.completed} onChange={handleOnChangeStatus}>
                        <option value="all">All</option>
                        <option value="completed">Completed</option>
                        <option value="uncompleted">Uncompleted</option>
                    </select>
                </div>
            </section>
        </>
    );
}
export default Filter2;
