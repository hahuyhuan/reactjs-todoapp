import ItemContainer from "../../containers/Todo/ItemContainer";

function Listing({ rows, thead=["Task", "Priority", "Status", "Controls"], children }) {

    return (
        <section className="listing">
            <table>
                <tbody>
                    <tr>
                        {thead && thead.map((col, index) => (
                            <th key={index}>{col}</th>
                        ))}
                    </tr>
                    {rows && rows.map((row, index) => (
                    <tr>
                        {rows && rows.map((col, index) => (
                            <td key={index}>{col}</td>
                        ))}
                    </tr>
                    ))}
                    {children}
                </tbody>
            </table>
        </section>
    )
}

export default Listing;
