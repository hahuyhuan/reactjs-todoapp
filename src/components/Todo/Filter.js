import { priorityLevel } from "../../constants/priority";
import InputText from "../InputText";
import Dropdown from "../Dropdown";
import DropdownItem from "../DropdownItem";

function Filter({
    filter,
    handleOnChangeSearchText,
    handleOnChangeSort,
    handleOnChangePriority,
    handleOnChangeStatus,
}) {
    return (
        <>
            <section className="filter">
                <div className="filter-item -full-width">
                    <label>Tìm kiếm</label>
                    <InputText
                        className="add-task-input"
                        placeholder="Nhập tên task"
                        value={filter.searchText}
                        onChange={handleOnChangeSearchText}
                    />
                </div>
                <div className="filter-item">
                    <label>Sort</label>
                    <Dropdown
                        value={filter.sort}
                        onChange={handleOnChangeSort}
                    >
                        <DropdownItem value="latest">Latest</DropdownItem>
                        <DropdownItem value="alphabet">alphabet</DropdownItem>
                    </Dropdown>
                </div>
                <div className="filter-item">
                    <label>Priority</label>
                    <Dropdown
                        value={filter.priority}
                        onChange={handleOnChangePriority}
                    >
                        {priorityLevel.map((item, index) => {
                            return (
                                <DropdownItem key={index} value={index}>
                                    {item.label}
                                </DropdownItem>
                            );
                        })}
                    </Dropdown>
                </div>
                <div className="filter-item">
                    <label>Status</label>
                    <Dropdown
                        value={filter.completed}
                        onChange={handleOnChangeStatus}
                    >
                        <DropdownItem value="all">All</DropdownItem>
                        <DropdownItem value="completed">Completed</DropdownItem>
                        <DropdownItem value="uncompleted">
                            Uncompleted
                        </DropdownItem>
                    </Dropdown>
                </div>
            </section>
        </>
    );
}
export default Filter;
