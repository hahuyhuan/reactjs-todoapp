import Dropdown from "../Dropdown";
import DropdownItem from "../DropdownItem";
function FilterItemDropdown({ label = "Filter by", value, options, onChangeValue}) {
    return (
        <div className="filter-item">
            <label>{label}</label>
            <Dropdown value={value} onChange={onChangeValue}>
                {options.map((item, index) => {
                    return (
                        <DropdownItem key={index} value={item.value}>
                            {item.label}
                        </DropdownItem>
                    );
                })}
            </Dropdown>
        </div>
    );
}

export default FilterItemDropdown;