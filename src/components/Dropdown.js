import { useState, useEffect, useRef } from "react";
import DropdownItem from "./DropdownItem";

function Dropdown({ value, onChange, children }) {
    
    value = !value && children ? children[0].props.value : value;

    const [isExpanded, setIsExpanded] = useState(false);

    const currentLabel = children
        ? children.find((child) => {
              return child.props.value == value;
          }).props.children
        : value;

    let data = {
        value: value,
        label: currentLabel,
    };

    const dropdownWrapper = useRef();

    const toggleDropdown = (e) => {
        setIsExpanded(!isExpanded);
    };

    const closeDropdown = (e) => {
        setIsExpanded(false);
    };

    const handleClickBtn = (e) => {
        e.preventDefault();
        console.log("handleClickBtn");
        toggleDropdown();
    };

    const handleClickItem = (e) => {
        console.log("clickItem");
        data = {
            value: e.target.getAttribute("value"),
            label: currentLabel,
        };
        onChange && onChange(data);
        closeDropdown();
    };

    const handleClickOutside = (event) => {
        if (
            dropdownWrapper.current &&
            !dropdownWrapper.current.contains(event.target)
        ) {
            closeDropdown();
        }
    };

    useEffect((e) => {
        document.addEventListener("mousedown", handleClickOutside);
        return () => {
            document.removeEventListener("mousedown", handleClickOutside);
        };
    }, []);

    console.log("Dropdown render");

    return (
        <div
            className={`dropdown ${isExpanded ? "is-expanded" : ""}`}
            ref={dropdownWrapper}
        >
            <button className="dropdown__btn" onClick={handleClickBtn}>
                {currentLabel}
            </button>
            <ul className={`dropdown__dropdown`}>
                {children &&
                    children.map((child) => {
                        if (child.props.value === value) {
                            return (
                                <DropdownItem
                                    key={child.props.value}
                                    value={child.props.value}
                                    selected="true"
                                    onClick={handleClickItem}
                                >
                                    {child.props.children}
                                </DropdownItem>
                            );
                        } else {
                            return (
                                <DropdownItem
                                    key={child.props.value}
                                    value={child.props.value}
                                    onClick={handleClickItem}
                                >
                                    {child.props.children}
                                </DropdownItem>
                            );
                        }
                    })}
            </ul>
        </div>
    );
}

export default Dropdown;
