import { useState, useRef, useEffect } from "react";

function useFilterHooks(todoList) {

    const [filter, setFilter] = useState({
        searchText: "",
        priority: null,
        completed: null,
        sort: "latest",
    });

    const setSearchText = (value) => {
        setFilter({ ...filter, searchText: value });
    };

    const setSort = (value) => {
        setFilter({ ...filter, sort: value });
    };
    const setPriority = (value) => {
        setFilter({ ...filter, priority: Number(value) });
    };
    const setCompleted = (value) => {
        setFilter({ ...filter, completed: value });
    };

    const getTodoListResult = () => {

        let resultList = [...todoList];

        if (filter.searchText != null && filter.searchText.length > 0) {
            resultList = resultList.filter((todo) => {
                return (
                    todo.title
                        .toLowerCase()
                        .includes(filter.searchText.toLowerCase()) === true
                );
            });
        }

        if (filter.sort !== null && filter.sort !== "latest") {
            resultList = resultList.sort((a, b) => {
                if (a.title.toUpperCase() < b.title.toUpperCase()) {
                    return -1;
                }
                if (a.title.toUpperCase() > b.title.toUpperCase()) {
                    return 1;
                }
                return 0;
            });
        }

        if (filter.priority !== null && filter.priority !== 0) {
            resultList = resultList.filter((todo) => {
                return todo.priority === filter.priority;
            });
        }

        if (filter.completed !== null && filter.completed !== "all") {
            let filterCompleted =
                filter.completed === "completed" ? true : false;
            resultList = resultList.filter((todo) => {
                return todo.completed === filterCompleted;
            });
        }

        return resultList;

    }

    // useEffect(
    //     (e) => {
    //         getTodoListResult();
    //     },
    //     [filter, todoList]
    // );

    console.log("filter render");

    return [filter, setSearchText, setSort, setPriority, setCompleted, getTodoListResult];
}

export default useFilterHooks;
