import { useState } from "react";
import { v4 as uuidv4 } from "uuid";
import { addTodo, updateTodo} from "../../actions/TodoActions";
import { validate, createErrorContent } from "../../utils/validateTodoForm";
import FormTask from "../../components/Todo/FormTask";
import { useDispatch } from "react-redux/es/hooks/useDispatch";

function FormTaskContainer({ todo, isEditing, toggleIsEditing }) {
    const dispatch = useDispatch();

    const [inputTitle, setInputTitle] = useState(todo ? todo.title : "");
    const [inputPriority, setInputPriority] = useState(
        todo ? todo.priority : "0"
    );
    const [errorContent, setErrorContent] = useState("");

    const handleClickAdd = (e) => {
        console.log("handleClickAdd");

        let errors = validate(inputTitle, inputPriority);
        setErrorContent(createErrorContent(errors));

        if (errors.length > 0) {
            return false;
        } else {
            dispatch(
                addTodo({
                    id: uuidv4(),
                    title: inputTitle,
                    priority: Number(inputPriority),
                    completed: false,
                })
            );
            resetForm();
        }
    };

    const handleClickCancel = () => {
        console.log("handleClickCancel");
        toggleIsEditing && toggleIsEditing();
    };
    const handleClickDone = (e) => {
        console.log("handleClickDone");

        let errors = validate(inputTitle, inputPriority);
        setErrorContent(createErrorContent(errors));

        if (errors.length > 0) {
            return false;
        } else {
            dispatch(
                updateTodo({
                    ...todo,
                    ...{
                        title: inputTitle,
                        priority: Number(inputPriority),
                    },
                })
            );
            toggleIsEditing && toggleIsEditing();
        }
    };

    const resetForm = () => {
        setInputTitle("");
        setInputPriority(0);
    };

    const handleTitleOnChange = (e) => {
        setInputTitle(e.target.value);
        console.log("handleTitleOnChange");
    };

    const handlePriortyOnChange = (data) => {
        setInputPriority(Number(data.value));
        console.log("handlePriortyOnChange");
    };

    console.log("form task hook runs");

    return (
        <>
            <FormTask
                inputTitleValue={inputTitle}
                inputTitleOnChange={handleTitleOnChange}
                inputPriorityValue={inputPriority}
                inputPriorityOnchange={handlePriortyOnChange}
                onClickAdd={handleClickAdd}
                onClickDone={handleClickDone}
                onClickCancel={handleClickCancel}
                errorContent={errorContent}
                isEditing={isEditing}
            />
        </>
    );
}

export default FormTaskContainer;
