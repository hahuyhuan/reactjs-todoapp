// import logo from './logo.svg';
import { useState } from "react";
import FilterContainer from "./FilterContainer";
import ListingContainer from "./ListingContainer";
import AddTaskContainer from "./AddTaskContainer";
import { useSelector } from "react-redux";

function TodoContainer() {

    const todoList = useSelector(state => state);

    const [todoResultList, setTodoResultList] = useState([...todoList]);

    const [isLoading, setIsLoading] = useState(false);

    const Loading = (e) => {
        return <div className="loading">Loading...</div>;
    };

    console.log("todo render");

    // console.log(todoList);

    return (
        <>
            {isLoading === true ? (
                <Loading></Loading>
            ) : (
                <div className="app">
                    <header className="header">
                        <h1>App Todo</h1>
                    </header>
                    <div className="todo">
                        <FilterContainer
                            setTodoResultList={setTodoResultList}
                        ></FilterContainer>
                        <ListingContainer
                            todoResultList={todoResultList}
                        ></ListingContainer>
                        <AddTaskContainer></AddTaskContainer>
                    </div>
                </div>
            )}
        </>
    );
}

export default TodoContainer;
