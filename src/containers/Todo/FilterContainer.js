import { useEffect } from "react";
import { priorityLevel } from "../../constants/priority";
import useFilterHooks from "../../hooks/useFilterHooks";
import FilterItemDropdown from "../../components/Todo/FilterItemDropdown";
import FilterItemText from "../../components/Todo/FilterItemText";
import { useSelector } from "react-redux";

function FilterContainer({ setTodoResultList }) {

    const todoList = useSelector(state => state);

    const [filter, setSearchText, setSort, setPriority, setCompleted, getTodoListResult] = useFilterHooks(todoList)

    const handleOnChangeSearchText = (e) => {
        setSearchText(e.target.value);
    };

    const handleOnChangeSort = (data) => {
        setSort(data.value);
    };
    const handleOnChangePriority = (data) => {
        setPriority(Number(data.value));
    };
    const handleOnChangeStatus = (data) => {
        setCompleted(data.value);
    };

    const sortOptions = [
        { value: "latest", label: "Latest"},
        { value: "alphabet", label: "Alphabet"}
    ];

    const priorityOptions = [];
    priorityLevel.map((item, index) => {
        priorityOptions.push({value: index, label: item.label})
    })

    const statusOptions = [
        { value: "all", label: "All"},
        { value: "completed", label: "Completed"},
        { value: "uncompleted", label: "Uncompleted"},
    ];

    // console.log(getTodoListResult());

    useEffect(
        (e) => {

            setTodoResultList(getTodoListResult());
        },
        [filter, todoList]
    );

    console.log("filter render");

    return (
        <>
            <section className="filter">
                <FilterItemText label={'Tìm kiếm'} value={filter.searchText} onChangeValue={handleOnChangeSearchText} />
                <FilterItemDropdown label={'Sort'} value={filter.sort} options={sortOptions} onChangeValue={handleOnChangeSort}/>
                <FilterItemDropdown label={'Priority'} value={filter.priority} options={priorityOptions} onChangeValue={handleOnChangePriority}/>
                <FilterItemDropdown label={'Status'} value={filter.completed} options={statusOptions} onChangeValue={handleOnChangeStatus}/>
            </section>
        </>
    );
}

export default FilterContainer;
