import { useState, useRef, useEffect } from "react";
import { priorityLevel } from "../../constants/priority";
import { useTodoStore } from "../../hooks/useTodoStore";
import Filter2 from "../../components/Todo/Filter2";
import useFilterHooks from "../../hooks/useFilterHooks";

function FilterContainer2({ setTodoResultList }) {

    const { todoList } = useTodoStore();

    const [filter, setSearchText, setSort, setPriority, setCompleted, getTodoListResult] = useFilterHooks(todoList)

    const handleOnChangeSearchText = (e) => {
        setSearchText(e.target.value);
    };

    const handleOnChangeSort = (data) => {
        setSort(data.target.value);
    };
    const handleOnChangePriority = (data) => {
        setPriority(Number(data.target.value));
    };
    const handleOnChangeStatus = (data) => {
        setCompleted(data.target.value);
    };

    // console.log(getTodoListResult());

    useEffect(
        (e) => {

            setTodoResultList(getTodoListResult());
        },
        [filter, todoList]
    );

    console.log("filter render");

    return (
        <Filter2
            filter={filter}
            handleOnChangeSearchText={handleOnChangeSearchText}
            handleOnChangeSort={handleOnChangeSort}
            handleOnChangePriority={handleOnChangePriority}
            handleOnChangeStatus={handleOnChangeStatus}
        />
    );
}

export default FilterContainer2;
