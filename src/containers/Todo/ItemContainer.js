import { useState } from "react";
import { deleteTodo, toggleTodoPriority } from "../../actions/TodoActions";
import FormTaskContainer from "./FormTaskContainer";
import Item from "../../components/Todo/Item";
import { useDispatch } from "react-redux/es/exports";

function ItemContainer({ todo }) {

    const dispatch = useDispatch();

    const [isEditing, setisEditing] = useState(false);

    const toggleIsEditing = () => {
        console.log("toggleIsEditing");
        setisEditing(!isEditing);
    };

    const handleClickDelete = () => {
        console.log("handleClickDelete");
        dispatch(deleteTodo(todo.id))
    };
    const handleOnChangeCompleted = () => {
        console.log("handleOnChangeCompleted");
        dispatch(toggleTodoPriority(todo.id));
    };
    const handleClickEdit = () => {
        console.log("handleClickEdit");
        toggleIsEditing();
    };

    console.log("item render");
    return (
        <>
            {isEditing ? (
                <tr>
                    <td colSpan={4}>
                        <FormTaskContainer
                            todo={todo}
                            isEditing={isEditing}
                            toggleIsEditing={toggleIsEditing}
                        />
                    </td>
                </tr>
            ) : (
                <Item
                    todo={todo}
                    handleOnChangeCompleted={handleOnChangeCompleted}
                    handleClickDelete={handleClickDelete}
                    handleClickEdit={handleClickEdit}
                />
            )}
        </>
    );
}

export default ItemContainer;
