import Listing from '../../components/Todo/Listing';
import ItemContainer from './ItemContainer';
function ListingContainer({ todoResultList }) {
    console.log('listing render');
    return (
        <>
            <Listing>
                {todoResultList.map((todo, index) => (
                    <ItemContainer key={todo.id} todo={todo}></ItemContainer>
                ))}
            </Listing>
        </>
    );
}

export default ListingContainer;
