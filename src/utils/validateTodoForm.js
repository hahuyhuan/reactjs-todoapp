export const validate = (inputTitle, inputPriority) => {
    let errors = [];

    if (!inputTitle || inputTitle.length <= 0) {
        errors.push("Title is empty");
    }
    if (!inputPriority || inputPriority === "0") {
        errors.push("Priority is empty");
    }
    return errors;
};

export const createErrorContent = (errors) => {
    let content ='';
    if (errors.length > 0) {
        content +='<ul>';
        errors.forEach((error) => {
            content += `<li>${error}</li>`;
        });
        content +='</ul>';
        return content;
    } else {
        return "";
    }
};