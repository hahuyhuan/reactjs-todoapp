const webpack = require('webpack');
const path = require("path");
const { merge } = require('webpack-merge')
const common = require('./webpack.common');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const MODE = "development";
module.exports = merge(common, {
    mode: MODE,
    devtool: 'inline-source-map',
    devServer: {
        static: {
            directory: './dist'
        },
        port: 8000
    },
    module: {
        rules: [{
            test: /\.(scss|sass)$/,
            use: [
                {
                    loader: 'style-loader'
                },
                {
                    loader: 'css-loader'
                },
                {
                    loader: 'sass-loader'
                }
            ]
        }]
    },
    watch: false,
    plugins: [
        // HotModuleReplacementPlugin: nó giúp tạo ra server riêng tự động reload khi có bất kỳ thay đổi nào từ các file hệ client của project/
        new webpack.HotModuleReplacementPlugin(),
        new HtmlWebpackPlugin({
            template: path.join(__dirname, "public", "index.html"),
        }),
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery"
        })
    ]
})
