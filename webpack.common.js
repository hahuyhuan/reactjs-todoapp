//require('dotenv').config();

const HtmlWebpackPlugin = require("html-webpack-plugin");
const path = require("path");
const webpack = require('webpack');
const fs = require('fs');

const dirSass = './src/assets/sass/';
const arEntrySass = [];

////https://stackoverflow.com/questions/2727167/how-do-you-get-a-list-of-the-names-of-all-files-present-in-a-directory-in-node-j
fs.readdirSync(dirSass).forEach(file => {
    if(file.indexOf('.scss') === -1) return;
    arEntrySass.push(dirSass + file);
})
///////

module.exports = {
    entry: [
        ...['./index.js'],
        ...arEntrySass
    ],
    output: {
        filename: 'js/index.js',
        path: path.resolve(__dirname, 'dist')
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                loader: 'babel-loader',
                exclude: [/node_modules/]
            }
        ]
    }
}
